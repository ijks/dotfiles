- Lemonbar
  * [ ] Power button
  * [ ] More styling for the bspwm info
  * [ ] Music info

- Bspwm
  * [ ] Styling
  * [ ] Tune sxhkd bindings
  * [ ] See if there is a way to open a standard set of programs in a standard layout (i.e. which desktop, how is it tiled, etc.) on login

- General
  * [ ] Use a display manager
  * [ ] Make Termite read colours from .Xresources (will have to be a shell script I suppose)
  * [ ] Figure out how to configure compton
  * [ ] rice dat shit
